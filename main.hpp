/*
 * test1.hpp
 *
 *  Created on: 18/05/2014
 *      Author: Wilbert
 */
#include "tivaCppLibrary/include/Gpio.hpp"
#include "tivaCppLibrary/include/clock.hpp"
#include "tivaCppLibrary/delays.hpp"
#include "tivaCppLibrary/interrupts.hpp"
#include "tivaCppLibrary/include/Gptimer.hpp"
//Para usar interrupciones


#include "tivaCppLibrary/interrupts.hpp"
#include "tivaCppLibrary/include/Core.hpp"

#include "tivaCppLibrary/include/Adc.h"

bool go = false;
unsigned short buff[2]={0,0};

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);


	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOE);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PE3::disableDigitalFunc();
	PE3::setAnalogMode();

	adc::enableClock(adc::converter::adc0);
	adc::setSampleRate(adc::converter::adc0, adc::configOptions::sampleRate::sample_500ksps);

	adc0seq0::disableSequencer();

	adc0seq0::selectTrigger(adc::configOptions::trigger::software);

	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							adc::configOptions::inputpin::ain0,
							adc::configOptions::tempsensor::ommitTempSensor,
							adc::configOptions::interruptEnable::sampleIntDisabled,
							adc::configOptions::endOfSequence::isntEndOfSample,
							adc::configOptions::differentialMode::ommitDiffMode);

	adc0seq0::sampleConfigure(adc::configOptions::sample::sample_1st,
							adc::configOptions::inputpin::ain0,
							adc::configOptions::tempsensor::ommitTempSensor,
							adc::configOptions::interruptEnable::sampleIntEnable,
							adc::configOptions::endOfSequence::isntEndOfSample,
							adc::configOptions::differentialMode::ommitDiffMode);

	adc0seq0::enableSequencer();


	timer0::enableClock();
//Desabilitmos el subtimer que queremos congfigurar
	timer0::disableSubTimer(gpTimer::subTimers::subTimerA);
	//Configuramos al subtimer
	timer0::config(gpTimer::subTimers::subTimerA,
			500,
			gpTimer::options::timerOptions::joiningMode::timers_dontJoinTimers,
			gpTimer::options::timerOptions::timerMode::periodic,
			gpTimer::options::timerOptions::countDirection::up,
			gpTimer::options::timerOptions::interrupts::timerAtimeout);
//Habilitamos el subtimer
	timer0::enableSubTimer(gpTimer::subTimers::subTimerA);
//Le decimos al CPU que abilite interrupciones en general
	core::IntEnableMaster();
//Y que permita a la del timer 0$ de 32 bits)
	core::enableInterrupt(core::interrupts::timer0a_simple);

	while(1)
	{
		if(go){
			//tomar datos

			//iniciar toma de muestras
			adc0seq0::softBeginSampling();
			//Esperar hasta que acabe de tomar las muestas
			while(!(adc0seq0::finishedSampling() ) );

			adc0seq0::takeData(buff);


		}

		go=false;

	}
}


void interruptFuncs::timer0SubA_isr(){
	//Para limpiar la flag
	timer0::clearInterrupt(gpTimer::options::timerOptions::interrupts::timerAtimeout);
	PF2::toogle();

	go=true;

	//tomar los datos
}
