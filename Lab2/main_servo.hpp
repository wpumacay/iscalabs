/*
 * test1.hpp
 *
 *  Created on: 18/05/2014
 *      Author: Wilbert
 */
#include "tivaCppLibrary/include/Gpio.hpp"
#include "tivaCppLibrary/include/clock.hpp"
#include "tivaCppLibrary/delays.hpp"
#include "tivaCppLibrary/interrupts.hpp"
#include "tivaCppLibrary/include/Gptimer.hpp"
//Para usar interrupciones

#include "tivaCppLibrary/include/Uart.h"

#include "tivaCppLibrary/interrupts.hpp"
#include "tivaCppLibrary/include/Core.hpp"

#include "tivaCppLibrary/include/Adc.h"
#include "tivaCppLibrary/appLibraries/servo/Servo.hpp"

Servo::Servo<Gpio::Port::GPIOPortC_APB,
			Gpio::Pin::pin4,
			gpTimer::timer0_wide,
			gpTimer::subTimers::subTimerA>motor1;


float angle=0;
u8 counter = 0;
u8 rByte=0;



int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);


	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOA);
	Gpio::enableClock(Gpio::peripheral::GPIOC);

	motor1.initPins();
	motor1.config(20000, 1100, 1900, 0, 180 );

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);



	PA0::enableAsDigital();
	PA0::setMode(Gpio::options::mode::alternate);
	PA0::setAlternateMode(Gpio::options::altModes::alt1);

	PA1::enableAsDigital();
	PA1::setMode(Gpio::options::mode::alternate);
	PA1::setAlternateMode(Gpio::options::altModes::alt1);


	UART0::enableClock();
	UART0::configUart(uart::configOptions::baudrate::baud_1000000);

	core::IntEnableMaster();
	core::enableInterrupt(core::interrupts::uart0);


	while(1)
	{
		PF2::toogle();
		motor1.setAngle(angle);






	}
}


void interruptFuncs::uart0rxtx_isr(){
	UART0::clearInterrupts(uart::configOptions::interrupts::receiveInt);


	rByte=UART0::receiveByte();
	UART0::sendByte(rByte);

	if(rByte==119){
			UART0::sendString(": Arriba");
			UART0::sendByte(13);
			UART0::sendByte(10);
			if (angle<270) angle=angle+20;

		}

		if(rByte==115){
					UART0::sendString(": Abajo");
					UART0::sendByte(13);
					UART0::sendByte(10);
					if (angle>0) angle=angle-20;

		}




}
