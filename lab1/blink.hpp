/*
 * test1.hpp
 *
 *  Created on: 18/05/2014
 *      Author: Wilbert
 */
#include "tivaCppLibrary/include/Gpio.hpp"
#include "tivaCppLibrary/include/clock.hpp"
#include "tivaCppLibrary/delays.hpp"
#include "tivaCppLibrary/interrupts.hpp"

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);


	Gpio::enableClock(Gpio::peripheral::GPIOF);

	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);


	while(1)
	{
		PF1::setHigh();
		delays::delay_ms(250);
		PF1::setLow();

		PF2::setHigh();
		delays::delay_ms(250);
		PF2::setLow();

		PF3::setHigh();
		delays::delay_ms(250);
		PF3::setLow();
	}
}
