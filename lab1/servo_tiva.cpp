
#include "tivaCppLibrary/include/Gpio.hpp"
#include "tivaCppLibrary/include/clock.hpp"
#include "tivaCppLibrary/delays.hpp"
#include "tivaCppLibrary/interrupts.hpp"
#include "tivaCppLibrary/include/GpTimer.hpp"
#include "tivaCppLibrary/include/Core.hpp"

float duty=0.1;

int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	Gpio::enableClock(Gpio::peripheral::GPIOF);
	Gpio::enableClock(Gpio::peripheral::GPIOC);

	PC6::enableAsDigital();
	PC6::setMode(Gpio::options::mode::alternate);
	PC6::setAlternateMode(Gpio::options::altModes::alt7);

    pwmW_1A::enableClock();
    pwmW_1A::config(20,duty,gpTimer::options::pwmOptions::outputLevel::inverted);


	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	while(1)
	{
		PF2::toogle();
		delays::delay_ms(100);
		pwmW_1A::setDuty(duty);
	}
}

