/*
 * test1.hpp
 *
 *  Created on: 18/05/2014
 *      Author: Wilbert
 */
#include "tivaCppLibrary/include/Gpio.hpp"
#include "tivaCppLibrary/include/clock.hpp"
#include "tivaCppLibrary/delays.hpp"
#include "tivaCppLibrary/interrupts.hpp"


void toogleLed(int led,int num)
{
	int i;
	for ( i= 0; i < num; i++) {
		switch (led) {
			case 1:
				PF1::setHigh();
				delays::delay_ms(250);
				PF1::setLow();
				break;
			case 2:
				PF2::setHigh();
				delays::delay_ms(250);
				PF2::setLow();
				break;

			case 3:
				PF3::setHigh();
				delays::delay_ms(250);
				PF3::setLow();
				break;
		}
	}
}


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);


	Gpio::enableClock(Gpio::peripheral::GPIOF);

	PF1::enableAsDigital();
	PF1::setMode(Gpio::options::mode::gpio);
	PF1::setIOmode(Gpio::options::IOmode::output);

	PF2::enableAsDigital();
	PF2::setMode(Gpio::options::mode::gpio);
	PF2::setIOmode(Gpio::options::IOmode::output);

	PF3::enableAsDigital();
	PF3::setMode(Gpio::options::mode::gpio);
	PF3::setIOmode(Gpio::options::IOmode::output);


	while(1)
	{
		toogleLed(1,2);
		toogleLed(2,4);
		toogleLed(3,6);
	}
}
