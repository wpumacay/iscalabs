/*
 * test1.hpp
 *
 *  Created on: 18/05/2014
 *      Author: Wilbert
 */
#include "tivaCppLibrary/include/Gpio.hpp"
#include "tivaCppLibrary/include/clock.hpp"
#include "tivaCppLibrary/delays.hpp"
#include "tivaCppLibrary/interrupts.hpp"

u32 *pointer;


int main()
{
	clock::clock::config(clock::configOptions::clockSource::mainOscilator,
						 clock::configOptions::clockRate::clock_80Mhz);

	pointer = (u32 *)(0x400fe608);
	*pointer |= (1<<5);

//	Gpio::enableClock(Gpio::peripheral::GPIOF);

	PF3::enableAsDigital();

	PF3::setMode(Gpio::options::mode::gpio);

	PF3::setIOmode(Gpio::options::IOmode::output);



	while(1)
	{
		PF3::toogle();
		delays::delay_ms(500);
	}
}
